package com.google.samples.erickrojasperez.alumnos_registro;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "activity_main";

    private EditText alumno;
    private EditText pasword;
    private Button BotonLogin;
    private Button BotonCancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        alumno = (EditText) findViewById(R.id.NombreAlumno);
        pasword = (EditText) findViewById(R.id.PaswordAlumno);
        BotonLogin = (Button) findViewById(R.id.LoginButton);
        BotonCancelar = (Button) findViewById(R.id.CancelButton);

        // Creando nuestro boton de Login
        BotonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((alumno.getText().toString().equals("admin")) && (pasword.getText().toString().equals("admin"))) {

                    Toast.makeText(getApplicationContext(), "Ingresando...", Toast.LENGTH_SHORT).show();
                    Log.i(TAG, "El Boton Login fue presionado");

                    // Conectar con RegistroAlumnosActivity
                    Intent intent = new Intent(getBaseContext(), RegistroAlumnos.class);
                    intent.putExtra("Alumno:", "Erick Rojas Pérez");
                    intent.putExtra("Edad:", "28 años");
                    startActivity(intent);

                } else {

                    Toast.makeText(getApplicationContext(), "Error de Autenticación", Toast.LENGTH_SHORT).show();
                    Log.e(TAG, "Error de Autenticacion en el Login ");
                }
            }
        });


        }

}

